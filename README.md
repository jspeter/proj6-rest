# Project 6: Brevet time calculator service

Simple listing service from project 5 stored in MongoDB database.

## Author 
Jacob Petersen <jpeter17@uoregon.edu>

## What is in this repository

You have a minimal implementation of Docker compose in DockerRestAPI folder, using which you can create REST API-based services (as demonstrated in class). 

## Functionality added

   * "http://0.0.0.0:5001/listAll" should return all open and close times in the database
   * "http://0.0.0.0:5001/listOpenOnly" should return open times only
   * "http://0.0.0.0:5001/listCloseOnly" should return close times only

   * "http://0.0.0.0:5001/listAll/csv" should return all open and close times in CSV format
   * "http://0.0.0.0:5001/listOpenOnly/csv" should return open times only in CSV format
   * "http://0.0.0.0:5001/listCloseOnly/csv" should return close times only in CSV format

   * "http://0.0.0.0:5001/listAll/json" should return all open and close times in JSON format
   * "http://0.0.0.0:5001/listOpenOnly/json" should return open times only in JSON format
   * "http://0.0.0.0:5001/listCloseOnly/json" should return close times only in JSON format

   * "http://0.0.0.0:5001/listOpenOnly/csv?top=3" should return top 3 open times only (in ascending order) in CSV format 
   * "http://0.0.0.0:5001/listOpenOnly/json?top=5" should return top 5 open times only (in ascending order) in JSON format
   * "http://0.0.0.0:5001/listCloseOnly/csv?top=6" should return top 5 close times only (in ascending order) in CSV format
   * "http://0.0.0.0:5001/listCloseOnly/json?top=4" should return top 4 close times only (in ascending order) in JSON format

   * Consumer application found at http://0.0.0.0:5000
