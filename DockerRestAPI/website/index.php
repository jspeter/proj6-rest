<html>
    <head>
        <title>CIS 322 REST-api demo: Laptop list</title>
    </head>

    <body>
        <h1>List of All Times</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service:5000/listAll');
            $allTimes = json_decode($json);
            $openTimes = $allTimes->opentimes;
            $closeTimes = $allTimes->closetimes;
            echo '<h2>Open Times</h2>';
            foreach ($openTimes as $l) {
                echo "<li>$l</li>";
            }
            echo '<h2>Close Times</h2>';
            foreach($closeTimes as $l) {
                echo "<li>$l</li>";
            }
            ?>
        </ul>
        <h1>List of Open Times</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service:5000/listAll');
            $allTimes = json_decode($json);
            $openTimes = $allTimes->opentimes;
            foreach ($openTimes as $l) {
                echo "<li>$l</li>";
            }
            ?>
        </ul>
        <h1>List of Close Times</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service:5000/listAll');
            $allTimes = json_decode($json);
            $closeTimes = $allTimes->closetimes;
            foreach($closeTimes as $l) {
                echo "<li>$l</li>";
            }
            ?>
        </ul>
    </body>
</html>
