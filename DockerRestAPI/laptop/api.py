import os

from flask import Flask
from flask_restful import Resource, Api

from pymongo import MongoClient

app = Flask(__name__)
api = Api(app)

client = MongoClient('db', 27017)
db = client.tododb

class Laptop(Resource):
    def get(self):
        return {
            'Laptops': ['Mac OS', 'Dell', 
            'Windozzee',
	    'Yet another laptop!'
            ]
        }

class listAll(Resource):
    def get(self):
        rslt = {
            'opentimes': [],
            'closetimes': []
        }

        _items = db.tododb.find()
        items = [item for item in _items]

        for item in items: 
            opentimes = item['opentimes']
            closetimes = item['closetimes']
            for i in range(len(opentimes)):
                rslt['opentimes'].append(opentimes[i])
                rslt['closetimes'].append(closetimes[i])

        return rslt

class listOpenOnly(Resource):
    def get(self):
        rslt = {}

        _items = db.tododb.find()
        items = [item for item in _items]

        for item in items: 
            opentimes = item['opentimes']
            for i in range(len(opentimes)):
                rslt['opentimes'].append(opentimes[i])

        return rslt

class listCloseOnly(Resource):
    def get(self):
        rslt = {}

        _items = db.tododb.find()
        items = [item for item in _items]

        for item in items: 
            closetimes = item['closetimes']
            for i in range(len(closetimes)):
                rslt['closetimes'].append(closetimes[i])

        return rslt

class listAllJSON(Resource):
    def get(self):
        rslt = {
            'opentimes': [],
            'closetimes': []
        }

        _items = db.tododb.find()
        items = [item for item in _items]

        for item in items: 
            opentimes = item['opentimes']
            closetimes = item['closetimes']
            for i in range(len(opentimes)):
                rslt['opentimes'].append(opentimes[i])
                rslt['closetimes'].append(closetimes[i])

        return rslt

class listOpenOnlyJSON(Resource):
    def get(self):
        rslt = {}

        _items = db.tododb.find()
        items = [item for item in _items]

        for item in items: 
            opentimes = item['opentimes']
            for i in range(len(opentimes)):
                rslt['opentimes'].append(opentimes[i])

        return rslt

class listCloseOnlyJSON(Resource):
    def get(self):
        rslt = {}

        _items = db.tododb.find()
        items = [item for item in _items]

        for item in items: 
            closetimes = item['closetimes']
            for i in range(len(closetimes)):
                rslt['closetimes'].append(closetimes[i])

        return rslt

class listAllCSV(Resource):
    def get(self):
        rslt = ''

        _items = db.tododb.find()
        items = [item for item in _items]

        for item in items: 
            opentimes = item['opentimes']
            closetimes = item['closetimes']
            for i in range(len(opentimes)):
                rslt += opentimes[i] + ','
                rslt += closetimes[i] + ','
            rslt = rslt[:-1] + '\n'

        return rslt

class listOpenOnlyCSV(Resource):
    def get(self):
        rslt = ''

        _items = db.tododb.find()
        items = [item for item in _items]

        for item in items:
            times = item['opentimes'] 
            for time in times: 
                rslt += time + ','
            rslt = rslt[:-1] + '\n'

        return rslt

class listCloseOnlyCSV(Resource):
    def get(self):
        rslt = ''

        _items = db.tododb.find()
        items = [item for item in _items]

        for item in items:
            times = item['closetimes'] 
            for time in times: 
                rslt += time + ','
            rslt = rslt[:-1] + '\n'

        return rslt

api.add_resource(Laptop, '/')

api.add_resource(listAll, '/listAll')
api.add_resource(listOpenOnly, '/listOpenOnly')
api.add_resource(listCloseOnly, '/listCloseOnly')

api.add_resource(listAllJSON, '/listAll/json')
api.add_resource(listOpenOnlyJSON, '/listOpenOnly/json')
api.add_resource(listCloseOnlyJSON, '/listCloseOnly/json')

api.add_resource(listAllCSV, '/listAll/csv')
api.add_resource(listOpenOnlyCSV, '/listOpenOnly/csv')
api.add_resource(listCloseOnlyCSV, '/listCloseOnly/csv')

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
